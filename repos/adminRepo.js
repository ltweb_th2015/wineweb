var db = require('../fn/db');

exports.login = (u) => {
	var sql = `select * from users where Username = '${u.username}' and Password = '${u.password}' and Role = 1`;
	return db.load(sql);
}


// exports.loadAllByCat = () => {
// 	var sql = `select cat.CatID, cat.CatName, sum(ProAmount) as totalAmount 
// 	from categories cat, products p 
// 	where cat.CatID = p.CatID 
// 	group by cat.CatID`;
// 	return db.load(sql);
// }

// exports.loadAllByProducer = () => {
// 	var sql = `select pcer.ProducerID, pcer.ProducerName, sum(ProAmount) as totalAmount 
// 	from producers pcer, products p 
// 	where pcer.ProducerID = p.ProducerID 
// 	group by pcer.ProducerID`;
// 	return db.load(sql);
// }

exports.loadAllCat = () => {
	var sql = `select * from categories`;
	return db.load(sql);
}

exports.loadAllProducer = () => {
	var sql = `select * from producers`;
	return db.load(sql);
}


exports.loadAllOrder = () => {
	var sql = `select * from orders`;
	return db.load(sql);
}

exports.removeCat = (CatId) => {
	var sql = `delete from categories where CatID = ${CatId}`;
	return db.save(sql);
}

exports.removeProByCat = (CatId) => {
	var sql = `delete from products where CatID = ${CatId}`;
	return db.save(sql);
}

exports.removePro = (ProId) => {
	var sql = `delete from products where ProID = ${ProId}`;
	return db.save(sql);
}

exports.removeProducer = (ProducerId) => {
	var sql = `delete from producers where ProducerID = ${ProducerId}`;
	return db.save(sql);
}

exports.removeProByProducer = (ProducerId) => {
	var sql = `delete from products where ProducerID = ${ProducerId}`;
	return db.save(sql);
}


exports.addProducer = (producerName) => {
	var sql = `insert into producers(ProducerName) values('${producerName}')`;
	return db.save(sql);
}

exports.addCategory = (catName) => {
	var sql = `insert into categories(CatName) values('${catName}')`;
	return db.save(sql);
}


exports.updateProducer = (producer) => {
	var sql = `update producers set ProducerName = '${producer.producerName}' where ProducerID = ${producer.producerId}`;
    return db.save(sql);
}

exports.updateCategory = (category) => {
	var sql = `update categories set CatName = '${category.catName}' where CatID = ${category.catId}`;
    return db.save(sql);
}

exports.loadProducerById = (id) => {
	var sql = `select * from producers where ProducerID = ${id}`;
	return db.load(sql);
}

exports.loadCategoryById = (id) => {
	var sql = `select * from categories where CatID = ${id}`;
	return db.load(sql);
}