var db = require('../fn/db');

exports.add = (cart, item) => {
    for (i = cart.length - 1; i >= 0; i--) {
        if (cart[i].ProId === item.ProId) {
            cart[i].Quantity += item.Quantity;
            return;
        }
    }
    cart.push(item);
}

exports.remove = (cart, proId) => {
    for (var i = cart.length - 1; i >= 0; i--) {
        console.log(typeof(cart[i].ProId));
        if (+proId === cart[i].ProId) {
            cart.splice(i, 1);
            return;
        }
    }
}


exports.addOrder = (o) => {
    var sql = `insert into orders(OrderDate, Total, UserID) values('${o.OrderDate}', ${o.Total}, ${o.UserID})`;
    return db.save(sql);
}

exports.savePro = (p) => {
    var sql = `insert into orderdetails(ProID, Quantity, Price, Amount, OrderID) values(${p.Product.ProID}, ${p.Quantity}, ${p.Product.ProPrice}, ${p.Amount}, ${p.OrderID})`;
    return db.save(sql);
}

// exports.savePro = (p) => {
//     var sql = `insert into orderdetails(OrderID) values(${p.OrderID})`;
//     return db.save(sql);
// }


exports.countAll = () => {
    var sql = `select count(*) as total from orders`;
    return db.load(sql);
}

exports.loadAll = () => {
    var sql = `select * from orders`;
    return db.load(sql);
}