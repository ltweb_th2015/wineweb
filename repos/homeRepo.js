var db = require('../fn/db');

var config = require('../config/config');
var sort = require('../fn/sort');


exports.proNew = (offset) => {
	var sql = `select * from products limit ${config.PRODUCTS_NEW} offset ${offset}`;
	return db.load(sql);
}

exports.mostview = () => {
	return new Promise((resolve, reject) => {
		var sql = `select * from products`;
		db.load(sql).then(rows => {
			var sortIncreaseSeen = sort.increaseSeen(rows);
			if(sortIncreaseSeen.length === 0) {
				resolve(null);
			} else {
				resolve(sortIncreaseSeen);
			}
		}).catch(err => {
			reject(err);
		});
	});
}

exports.bestseller = () => {
	return new Promise((resolve, reject) => {
		var sql = `select * from products`;
		db.load(sql).then(rows => {
			var sortIncreaseBuy = sort.increaseBuy(rows);
			if(sortIncreaseBuy.length === 0) {
				resolve(null);
			} else {
				resolve(sortIncreaseBuy);
			}
		}).catch(err => {
			reject(err);
		});
	});
}
