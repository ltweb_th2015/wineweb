var db = require('../fn/db');
var config = require('../config/config');

exports.loadAll = () => {
	var sql = `select * from products`;
	return db.load(sql);
}

exports.loadPage = (offset) => {
	var sql = `select * from products limit ${config.PRODUCTS_PER_PAGE} offset ${offset}`;
	return db.load(sql);
}

exports.loadAllByCat = (catId, offset) => {
	var sql = `select * from products where CatID = ${catId} limit ${config.PRODUCTS_PER_PAGE} offset ${offset}`;
	return db.load(sql);
}

exports.loadAllByProducer = (producerId, offset) => {
	var sql = `select * from products where ProducerID = ${producerId} limit ${config.PRODUCTS_PER_PAGE} offset ${offset}`;
	return db.load(sql);
}


exports.countAll = () => {
	var sql = `select count(*) as total from products`;
	return db.load(sql);
}

exports.countAllCat = (catId) => {
	var sql = `select count(*) as total from products where CatID = ${catId}`;
	return db.load(sql);
}

exports.countAllProducer = (producerId) => {
	var sql = `select count(*) as total from products where ProducerID = ${producerId}`;
	return db.load(sql);
}

exports.singledetail = (proId) => {
	return new Promise((resolve, reject) => {
		var sql = `select * 
		from products p 
		join producers pro on p.ProducerID = pro.ProducerID 
		join categories c on p.CatID = c.CatID 
		where p.ProID = ${proId}`;
		db.load(sql).then(rows =>{
			if(rows.length === 0){
				resolve(null);
			} else {
				resolve(rows[0]);
			}
		}).catch(err => {
			reject(err);
		});
	});
}

exports.loadAllByCatDetail = (proId, catId, offset) => {
	var sql = `select * from products where CatID = ${catId} and ProID not like ${proId} limit ${config.PRODUCTS_PER_DETAIL} offset ${offset}`;
	return db.load(sql);
}

exports.loadAllByProducerDetail = (proId, producerId, offset) => {
	var sql = `select * from products where ProducerID = ${producerId} and ProID not like ${proId} limit ${config.PRODUCTS_PER_DETAIL} offset ${offset}`;
	return db.load(sql);
}


exports.single = (proId) => {
	return new Promise((resolve, reject) => {
		var sql = `select * from products where ProID = ${proId}`;
		db.load(sql).then(rows => {
			if(rows.length === 0) {
				resolve(null);
			} else {
				resolve(rows[0]);
			}
		}).catch(err => {
			reject(err);
		});
	});
}

exports.loadsession = (offset) => {
	var sql = `select * from products limit ${config.PRODUCTS_NEW} offset ${offset}`;
	return db.load(sql);
}


exports.increaseSee = (pro) => {
	var sql = `update products set AmountSeen = ${pro.AmountSeen + 1} where ProID = ${pro.ProID}`;
    return db.save(sql);
}

exports.updateAmount = (pro, numbers) => {
	var sql = `update products set ProAmount = ${pro.ProAmount - numbers}, AmountBuy = ${pro.AmountBuy + numbers} where ProID = ${pro.ProID}`;
    return db.save(sql);
}