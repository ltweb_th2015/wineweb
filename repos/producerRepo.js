var db = require('../fn/db')

exports.loadAll = () => {
	var sql = `select * from producers`;
	return db.load(sql);
}

exports.single = (producerId) => {
	return new Promise((resolve, reject) => {
		var sql = `select * from producers where ProducerID = ${producerId}`;
		db.load(sql).then(rows => {
			if(rows.length === 0) {
				resolve(null);
			} else {
				resolve(rows[0]);
			}
		}).catch(err => {
			reject(err);
		});
	});
}