exports.increaseSeen = (rows) => {
	var arr = [];
	for(var i = 0; i < rows.length; i++) {
		for(var j = i + 1; j < rows.length; j++) {
			if(rows[i].AmountSeen > rows[j].AmountSeen) {
				var temp = rows[i];
				rows[i] = rows[j];
				rows[j] = temp;
			}
		}
		arr[i] = rows[i];
	}
	return arr;
}

exports.increaseBuy = (rows) => {
	var arr = [];
	for(var i = 0; i < rows.length; i++) {
		for(var j = i + 1; j < rows.length; j++) {
			if(rows[i].AmountBuy > rows[j].AmountBuy) {
				var temp = rows[i];
				rows[i] = rows[j];
				rows[j] = temp;
			}
		}
		arr[i] = rows[i];
	}
	return arr;
}