var mysql = require('mysql');

exports.load = sql => {
	return new Promise((resolve, reject) => {
		var connection = mysql.createConnection({
			host: '127.0.0.1',
			port: '3306',
			user: 'root',
			password: '',
			database: 'webwine1'
		});
		connection.connect();
		connection.query(sql, function(error, rows, fields){
			if(error){
				reject(error);
			} else {
				resolve(rows);
			}
			connection.end();
		});
	});
}

exports.save = sql =>{
	return new Promise((resolve, reject) => {
		var connection = mysql.createConnection({
			host: '127.0.0.1',
			port: '3306',
			user: 'root',
			password: '',
			database: 'webwine1'
		});
		connection.connect();
		connection.query(sql, function(error, value){
			if(error){
				reject(error);
			} else {
				resolve(value);
			}
			connection.end();
		});
	});
}