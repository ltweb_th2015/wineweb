var express = require('express');

var homeRepo = require('../repos/homeRepo'),
	productRepo = require('../repos/productRepo');

var sort = require('../fn/sort');

var router = express.Router();

router.get('/', (req, res) => {
	productRepo.countAll().then(row => {
		var offset = row[0].total - 8;
		var mostview_p = [];
		var bestseller_p = [];
		var p1 = homeRepo.proNew(offset);
		var p2 = homeRepo.mostview();
		var p3 = homeRepo.bestseller();
		Promise.all([p1, p2, p3]).then(([pRows1, pRows2, pRows3]) => {
			for(var i = offset; i < row[0].total; i++) {
				mostview_p.push(pRows2[i]);
				bestseller_p.push(pRows3[i]);
			}
			var vm = {
				newProducts: pRows1,
				mostviewP: mostview_p,
				bestsellerP: bestseller_p
			};
			res.render('home/index', vm);
		});
	});
});


module.exports = router;