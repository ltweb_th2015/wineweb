var express = require('express');
var orderRepo = require('../repos/orderRepo');
var restrict = require('../middle-wares/restrict');

var router = express.Router();

router.get('/order', restrict, (req, res) => {
	orderRepo.loadAll(req.session.user.UserID).then(rows => {
		var vm = {
			orders: rows,
			user: req.session.user
		};
		res.render('history/order', vm);
	});
});

router.get('/order/:orderId', restrict, (req, res) => {
	var p1 = orderRepo.single(req.params.orderId);
	var p2 = orderRepo.detailOrder(req.params.orderId);
	Promise.all([p1, p2]).then(([orow, prows]) => {
		var vm = {
			products: prows,
			user: req.session.user,
			order: orow[0]
		};
		res.render('history/order-detail', vm);
	});
});

module.exports = router;