var express = require('express');
var cartRepo = require('../repos/cartRepo');
var productRepo = require('../repos/productRepo');
var restrict = require('../middle-wares/restrict');

var router = express.Router();

router.get('/', restrict, (req, res) => {
	var arr_p = [];
	for(var i = 0; i< req.session.cart.length; i++) {
		var cartItem = req.session.cart[i];
		var p = productRepo.single(cartItem.ProId);
		arr_p.push(p);
	}

	var items = [];
	Promise.all(arr_p).then(result => {
		for(var i = result.length - 1; i >= 0; i--) {
			var pro = result[i];
			var item = {
				Product: pro,
				Quantity: req.session.cart[i].Quantity,
				Amount: pro.ProPrice * req.session.cart[i].Quantity
			};
			items.push(item);
		}
		var total = 0;
		for(var i = 0; i < items.length; i++) {
			total = total + items[i].Amount;
		}

		var vm = {
			items: items,
			total: total
		};
		res.render('cart/index', vm);
	});
});

router.post('/add', (req, res) => {
	var item = {
		ProId: +req.body.proId,
		Quantity: +req.body.quantity
	};
	cartRepo.add(req.session.cart, item);
	res.redirect('/shop');
});

router.post('/remove', (req, res) => {
    cartRepo.remove(req.session.cart, req.body.ProId);
    res.redirect(req.headers.referer);
});

router.post('/order', (req, res) => {
	if(req.session.cart.length <= 0) {
		var vm = {
			NotOrder: true,
			setup: true,
			ErrorMsg: 'Order failed'
		};
		res.render('cart/index', vm);
	} else {
		var order = {
			OrderDate: req.body.OrderDate,
			Total: req.body.Total,
			UserID: req.session.user.UserID
		};
		cartRepo.addOrder(order).then(row => {
			var orderid = row.insertId;
			var arr_p = [];
			for(var i = 0; i< req.session.cart.length; i++) {
				var cartItem = req.session.cart[i];
				var p = productRepo.single(cartItem.ProId);
				arr_p.push(p);
			}
			Promise.all(arr_p).then(result => {
				var arr = [];
				for(var i = result.length - 1; i >= 0; i--) {
					var pro = result[i];
					var item = {
						OrderID: orderid,
						Product: pro,
						Quantity: req.session.cart[i].Quantity,
						Amount: pro.ProPrice * req.session.cart[i].Quantity
					};
					cartRepo.savePro(item);
					productRepo.updateAmount(pro, req.session.cart[i].Quantity);
				}
				req.session.cart = [];
				var vm = {
					showSuccess: true,
					setup: true,
					successMsg: 'Order success'
				}
				res.render('cart/index', vm);
			});
		});
	}
});

module.exports = router;