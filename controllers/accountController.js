var express = require('express'),
	SHA256 = require('crypto-js/sha256');

var accountRepo = require('../repos/accountRepo');
var restrict = require('../middle-wares/restrict');

var router = express.Router();

router.get('/login', (req, res) => {
	res.render('account/login');
});

router.post('/login', (req, res) => {
	var user = {
		username: req.body.username,
		password: SHA256(req.body.password).toString()
	};
	accountRepo.login(user).then(row => {
		if(row.length > 0){
			req.session.isLogged = true;
			req.session.user = row[0];
			req.session.cart = [];
			res.redirect('/');
		} else {
			var vm = {
				showError: true,
				errorMsg: 'Login failed'
			};
			res.render('account/login', vm);
		}
	});
});


router.get('/register', (req, res) => {
	res.render('account/register');
});

router.post('/register', (req, res) => {
	var user = {
		username: req.body.username,
		fullname: req.body.fullname,
		phonenumber: req.body.phonenumber,
		email: req.body.email,
		address: req.body.address,
		password: SHA256(req.body.password).toString(),
		role: 0
	};
	accountRepo.add(user).then(value => {
		var vm = {
			showSuccess: true,
			successMsg: 'Register success'
		};
		res.render('account/login', vm);
	});
});

router.post('/logout', (req, res) => {
    req.session.isLogged = false;
    req.session.user = null;
    // req.session.cart = [];
    res.redirect('/');
});

router.get('/profile', restrict, (req, res) => {
	var vm = {
		user: req.session.user
	};
	res.render('account/profile', vm);
});

router.post('/profile', (req, res) => {
	var user = {
		fullname: req.body.fullname,
		address: req.body.address,
		userid: req.body.userid
	};
	accountRepo.update(user).then(value => {
		accountRepo.loadUserById(req.body.userid).then(rows => {
			var vm = {
				user: rows[0]
			};
			res.render('account/profile', vm);
		});
	});
});

router.get('/payment', restrict, (req, res) => {
	res.render('home/payment');
});

module.exports = router;