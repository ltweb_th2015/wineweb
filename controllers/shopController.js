var express = require('express');

var categoryRepo = require('../repos/categoryRepo'),
	producerRepo = require('../repos/producerRepo'),
	productRepo = require('../repos/productRepo'),
	config = require('../config/config');

var router = express.Router();

router.get('/', (req, res) => {
	var page = req.query.page;
	if(!page){
		page = 1
	}
	var offset = (page - 1) * config.PRODUCTS_PER_PAGE;

	var p1 = categoryRepo.loadAll();
	var p2 = producerRepo.loadAll();
	var p3 = productRepo.loadPage(offset);
	var p4 = productRepo.countAll();

	Promise.all([p1, p2, p3, p4]).then(([categoryRows, producerRows, productRows, countRows]) => {
		var total = countRows[0].total;
		var nPages = total / config.PRODUCTS_PER_PAGE;
		if(total % config.PRODUCTS_PER_PAGE > 0) {
			nPages++;
		}
		var numbers = [];
		for(i = 1; i <= nPages; i++){
			numbers.push({
				value: i,
				isCurPage: i === +page
			});
		}

		var vm = {
			categories: categoryRows,
			producers: producerRows,
			products: productRows,
			page_numbers: numbers
		};
		res.render('shop/product', vm);
	});
});

router.get('/byCat/:id', (req, res) => {
	var id = req.params.id;
	var page = req.query.page;
	if(!page){
		page = 1;
	}
	var offset = (page - 1) * config.PRODUCTS_PER_PAGE;

	var p1 = categoryRepo.loadAll();
	var p2 = producerRepo.loadAll();
	var p3 = productRepo.loadAllByCat(id, offset);
	var p4 = productRepo.countAllCat(id);

	Promise.all([p1, p2, p3, p4]).then(([categoryRows, producerRows, productRows, countRows]) => {
		var total = countRows[0].total;
		var nPages = total / config.PRODUCTS_PER_PAGE;
		if(total % config.PRODUCTS_PER_PAGE > 0){
			nPages++;
		}

		var numbers = [];
		for(i = 1; i <= nPages; i++){
			numbers.push({
				value: i,
				isCurPage: i === +page
			});
		}
		var vm = {
			categories: categoryRows,
			producers: producerRows,
			products: productRows,
			page_numbers: numbers
		};
		res.render('shop/product', vm);
	});
});

router.get('/byProducer/:id', (req, res) => {
	var id = req.params.id;
	var page = req.query.page;
	if(!page){
		page = 1;
	}

	var offset = (page - 1) * config.PRODUCTS_PER_PAGE;
	var p1 = categoryRepo.loadAll();
	var p2 = producerRepo.loadAll();
	var p3 = productRepo.loadAllByProducer(id, offset);
	var p4 = productRepo.countAllProducer(id);

	Promise.all([p1, p2, p3, p4]).then(([categoryRows, producerRows, productRows, countRows]) => {
		var total = countRows[0].total;
		var nPages = total / config.PRODUCTS_PER_PAGE;
		if(total % config.PRODUCTS_PER_PAGE > 0){
			nPages++;
		}

		var numbers = [];
		for(i = 1; i <= nPages; i++){
			numbers.push({
				value: i,
				isCurPage: i === +page
			});
		}

		var vm = {
			categories: categoryRows,
			producers: producerRows,
			products: productRows,
			page_numbers: numbers
		};
		res.render('shop/product', vm);
	});
});

router.get('/productId/:proId/:catId/:producerId', (req, res) => {
	var proId = req.params.proId;
	var producerId = req.params.producerId;
	var catId = req.params.catId;
	var offset = 0;
	var p1 = productRepo.loadAllByCatDetail(proId, catId, offset);
	var p2 = productRepo.loadAllByProducerDetail(proId, producerId, offset);
	var p3 = productRepo.singledetail(proId);
	Promise.all([p1, p2, p3]).then(([cRows, pRows, product]) => {
		var p = productRepo.increaseSee(product);
		var vm = {
			catProducts: cRows,
			proProducts: pRows,
			product: product
		};
		res.render('shop/productDetail', vm);
	});
});


module.exports = router;