var express = require('express'),
	SHA256 = require('crypto-js/sha256');

var productRepo = require('../repos/productRepo'),
	producerRepo = require('../repos/producerRepo'),
	productTypeRepo = require('../repos/categoryRepo'),
	orderRepo = require('../repos/orderRepo'),
	adminRepo = require('../repos/adminRepo'),
	restrictAdmin = require('../middle-wares/restrictAdmin');

var router = express.Router();

router.get('/', (req, res) => {
	res.render('admin/admin', {layout: 'admin-layout.handlebars'});
});

// tính năng login

router.get('/login', (req,res) => {
	res.render('admin/login', {layout: 'admin-layout.handlebars'});
});

router.post('/login', (req, res) => {
	var admin = {
		username: req.body.username,
		password: SHA256(req.body.password).toString()
	};
	adminRepo.login(admin).then(row => {
		if(row.length > 0){
			req.session.isLogged = true;
			req.session.admin = row[0];
			res.redirect('/admin');
		} else {
			var vm = {
				showError: true,
				errorMsg: 'Login failed',
				layout: 'admin-layout.handlebars'
			};
			res.render('admin/login', vm);
		}
	});
});


// Tính năng logout

router.post('/logout', (req, res) => {
    req.session.isLogged = false;
    req.session.user = null;
    res.redirect('/admin');
});



router.get('/order_management', restrictAdmin, (req, res) => {
	adminRepo.loadAllOrder().then(rows => {
		var vm = {
			orders: rows,
			layout: 'admin-layout.handlebars'
		};
		res.render('admin/order-management', vm);
	});
});

router.get('/product_details', restrictAdmin, (req, res) => {
	res.render('admin/product-details', {layout: 'admin-layout.handlebars'});
});

// quản lý nhà sản xuất

router.get('/producer_management', restrictAdmin, (req, res) => {
	adminRepo.loadAllProducer().then(rows => {
		var vm = {
			producer: rows,
			layout: 'admin-layout.handlebars'
		};
		res.render('admin/producer-management', vm);
	});
});

router.post('/removeProducer', (req, res) => {
	adminRepo.removeProducer(req.body.ProducerId);
	adminRepo.removeProByProducer(req.body.ProducerId);
	res.redirect(req.headers.referer);
});

router.get('/edit/producer', (req, res) => {
	var producerId = req.query.id;
	adminRepo.loadProducerById(producerId).then(rows => {
		var vm = {
			producer: rows[0],
			layout: 'admin-layout.handlebars'
		};
		res.render('admin/edit-producer', vm);
	});
});

router.get('/add/producer', (req, res) => {
	res.render('admin/add-producer', {layout: 'admin-layout.handlebars'});
});

router.post('/add/producer', (req, res) => {
	var producerName = req.body.NameProducer;
	adminRepo.addProducer(producerName).then(values => {
		res.redirect('/admin/producer_management');
	});
});

router.post('/edit/producer', (req, res) => {
	var producer = {
		producerName: req.body.producerName,
		producerId: +req.body.producerId
	};
	adminRepo.updateProducer(producer).then(values => {
		res.redirect('/admin/producer_management');
	});
});


// quản lý sản phẩm

router.get('/product_management', restrictAdmin, (req, res) => {
	var products = productRepo.loadAll();
	var producers = producerRepo.loadAll();
	var productTypes = productTypeRepo.loadAll();
	Promise.all([products, producers, productTypes]).then(([productRows, producerRows, productTypeRows]) => {
		var vm = {
			products: productRows,
			producers: producerRows,
			productTypes: productTypeRows,
			layout: 'admin-layout.handlebars'
		};
		res.render('admin/product-management', vm);
	});
});

router.post('/removeProduct', (req, res) => {
	adminRepo.removePro(req.body.ProId);
	res.redirect(req.headers.referer);
});

// quản lý loại sản phẩm

router.get('/product_type_management', restrictAdmin, (req, res) => {
	adminRepo.loadAllCat().then(rows => {
		var vm = {
			productTypes: rows,
			layout: 'admin-layout.handlebars'
		};
		res.render('admin/product-type-management', vm);
	});
});

router.post('/removeCat', (req, res) => {
	adminRepo.removeCat(req.body.CatId);
	adminRepo.removeProByCat(req.body.CatId);
	res.redirect(req.headers.referer);
});


router.get('/edit/typeproduct', (req, res) => {
	var catId = req.query.id;
	adminRepo.loadCategoryById(catId).then(rows => {
		var vm = {
			category: rows[0],
			layout: 'admin-layout.handlebars'
		};
		res.render('admin/edit-type-product', vm);
	});
});

router.get('/add/typeproduct', (req, res) => {
	res.render('admin/add-type-product', {layout: 'admin-layout.handlebars'});
});

router.post('/add/typeproduct', (req, res) => {
	var catName = req.body.catName;
	adminRepo.addCategory(catName).then(values => {
		res.redirect('/admin/product_type_management');
	});
});

router.post('/edit/typeproduct', (req, res) => {
	var category = {
		catName: req.body.catName,
		catId: +req.body.catId
	};
	adminRepo.updateCategory(category).then(values => {
		res.redirect('/admin/product_type_management');
	});
});


module.exports = router;