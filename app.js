var express = require('express');
var exphbs = require('express-handlebars');
var express_handlebars_sections = require('express-handlebars-sections');
var bodyParser = require('body-parser');
var wnumb = require('wnumb');
var path = require('path');

var session = require('express-session');
var MySQLStore = require('express-mysql-session')(session);

var handle404MDW = require('./middle-wares/handle404'),
	handleLayoutMDW = require('./middle-wares/handleLayout');

var shopController = require('./controllers/shopController'),
	homeController = require('./controllers/homeController'),
	accountController = require('./controllers/accountController'),
	cartController = require('./controllers/cartController'),
	historyController = require('./controllers/historyController');
	adminController = require('./controllers/adminController.js');

var app = express();

app.engine('hbs', exphbs({
	defaultLayout: 'main',
	layoutsDir: 'views/_layout/',
	helpers: {
		section: express_handlebars_sections(),
		number_format: n => {
			var nf = wnumb({
				thousand: ','
			});
			return nf.to(n);
		}
	}
}));

app.set('view engine', 'hbs');

app.use(express.static(path.resolve(__dirname, 'public')));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
	extended: false
}));

// session

var sessionStore = new MySQLStore({
    host: '127.0.0.1',
    port: 3306,
    user: 'root',
    password: '',
    database: 'webwine',
    createDatabaseTable: true,
    schema: {
        tableName: 'sessions',
        columnNames: {
            session_id: 'session_id',
            expires: 'expires',
            data: 'data'
        }
    }
});

app.use(session({
    key: 'session_cookie_name',
    secret: 'session_cookie_secret',
    store: sessionStore,
    resave: false,
    saveUninitialized: false
}));

app.use(handleLayoutMDW);

app.get('/', (req, res) => {
	res.redirect('/home');
});

app.use('/home', homeController);
app.use('/shop', shopController);
app.use('/account', accountController);
app.use('/cart', cartController);
app.use('/history', historyController);
app.use('/admin', adminController);


app.use(handle404MDW);

app.listen(8080, () => {
	console.log('Site running on port 8080');
})